//
//  GameButtonView.swift
//  Katsu
//
//  Created by Sarah Crowle on 2022-09-07.
//

import SwiftUI

struct GameButtonView: View {
    @State private var selected = false
    @State public var name = "!!!ERR!!!"
    @State public var size = 0
    
    func selectGame() {
        selected.toggle()
    }
    
    var body: some View {
        Button(action: selectGame) {
            VStack {
                HStack(alignment: .firstTextBaseline){
                    Image(systemName: "globe")
                        .imageScale(.large)
                        .foregroundColor(.accentColor)
                    Spacer()
                    VStack {
                        Text(name)
                        Text(String(size) + " GB")
                            .font(.footnote)
                    }
                    Spacer()
                    Button {
                        print("hello!")
                    } label: {
                        Image(systemName: "play")
                            .imageScale(.large)
                    }
                }
            }
            .padding()
            .background(selected ? Color.accentColor.opacity(0.2) : Color.gray.opacity(0))
        }.buttonStyle(.plain)
         .frame(maxWidth: .infinity)
    }
}

struct GameButtonView_Previews: PreviewProvider {
    static var previews: some View {
        GameButtonView()
    }
}
