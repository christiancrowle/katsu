//
//  KatsuApp.swift
//  Katsu
//
//  Created by Sarah Crowle on 2022-09-07.
//

import SwiftUI

@main
struct KatsuApp: App {    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
