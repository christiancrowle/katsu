//
//  ContentView.swift
//  Katsu
//
//  Created by Sarah Crowle on 2022-09-07.
//

import SwiftUI

enum WineBuild: String, CaseIterable, Identifiable {
    case wine_vanilla, wine_tkg
    var id: Self { self }
}

struct ContentView: View {
    @State private var selectedWine : WineBuild = .wine_vanilla;
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            VStack(alignment: .trailing) {
                GameButtonView(name: "Final Fantasy XIV",
                               size: 80)
                GameButtonView(name: "Civilization V",
                               size: 15)
                GameButtonView(name: "The Elder Scrolls V: Skyrim",
                               size: 30)
                GameButtonView()
                GameButtonView()
                GameButtonView()
            }.padding()
            VStack {
                Text("Katsu!").font(.largeTitle)
                Text("a WINE frontend for Apple Silicon").font(.caption2)
                VStack {
                    Text("Game Settings").font(.title3)
                    Picker("WINE Build", selection: $selectedWine) {
                        ForEach (WineBuild.allCases) { wineBuild in
                            Text(wineBuild.rawValue.replacingOccurrences(of: "_", with: " ").capitalized)
                        }
                    }
                }.padding()
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
