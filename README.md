# Katsu

Katsu is a wine frontend built for Apple Silicon Macs.

## License

Wine is under the LGPL. The Katsu frontend is under MIT (see LICENSE).
